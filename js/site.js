$(document).ready(function(){
    if($("#reviewSlider").length > 0){
        (function(){
            var ind, newInd;
            $(".reviewSlide").each(function(){
                $("#reviewSlider-pagination").append("<span />");
            });
            $(".reviewSlide").css("top","-100%").eq(0).animate({
                "top": "0"
            },250);

            $("#reviewSlider-pagination span").eq(0).addClass("active");
            
            $("#reviewSlider-pagination span").click(function(){
                ind = $("#reviewSlider-pagination span.active").index();
                $("#reviewSlider-pagination span").removeClass("active");                
                $(this).addClass("active");
                newInd = $(this).index();

                $(".reviewSlide").eq(ind).animate({"top":"100%"},150,function(){
                    $(".reviewSlide").eq(ind).css("top","-100%");
                    $(".reviewSlide").eq(newInd).animate({
                        "top":"0"
                    },250);
                });

            });

            setInterval(function(){
                ind = $("#reviewSlider-pagination span.active").index();
                newInd = ind + 1;
                if(newInd >= $("#reviewSlider-pagination span").length){
                    newInd = 0;
                }

                $("#reviewSlider-pagination span").removeClass("active").eq(newInd).addClass("active");

                $(".reviewSlide").eq(ind).animate({"top":"100%"},150,function(){
                    $(".reviewSlide").eq(ind).css("top","-100%");
                    $(".reviewSlide").eq(newInd).animate({
                        "top":"0"
                    },250);
                });
            },4000);

        })();
    }

    (function(){
        if($("#mainMenu > ul").children("li").length > 2){
            $("<span id='hamburger' />").prependTo("#mainMenu");
            $("#header").addClass("withHamburger");

            $("#hamburger").click(function(){
                $("#mainMenu > ul").slideToggle(200);
            });
        }
    })();

    $(".showSubMenu").click(function(){
        if(window.innerWidth < 1024){
            $(this).parent().children("ul").slideToggle(150);
        }
    });

    $(window).resize(function(){
        if(window.innerWidth > 1024){
            $("#mainMenu ul ul").removeAttr("style");
        }
    });

    if(typeof lightbox != "undefined"){
        lightbox.option({
            "showImageNumberLabel": false,
            "alwaysShowNavOnTouchDevices": true
        });
    }

    $(".showHideLabel").click(function(){
        $(this).next(".showHideContent").slideToggle(150);
        $(this).parents(".showHideBlock").toggleClass('displayed');
    });

    $(".togglePass").bind("mousedown",function(){
        $(this).addClass("show").prev("input").attr("type","text");
    }).bind("mouseup",function(){
        $(this).removeClass("show").prev("input").attr("type","password");
    });

    /* form calc */
    if($(".powerCalc").length > 0){
        $(".calcChk").each(function(){
            if($(this).is(":checked")){
                $(this).parents(".calcRow").removeClass("disabled").find("input[type=text],select").removeAttr("disabled");
            }else{
                $(this).parents(".calcRow").addClass("disabled").find("input[type=text],select").attr("disabled","disabled");
            }
        });
        $(".calcRow .styledChk").on("change",function(){
            if($(this).is(":checked")){
                $(this).parents(".calcRow").removeClass("disabled").find("input[type=text],select").removeAttr("disabled");
            }else{
                $(this).parents(".calcRow").addClass("disabled").find("input[type=text],select").attr("disabled","disabled");
            }
        });
    }
    /* end form calc */

    var is16 = false;
    var important_keys = [8,37,39,46];
    var keyCodes = [48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];

    $(".check-phone").each(function(){
        $(this).bind("blur.check",function(){
            if($(this).val().match(/^\+[0-9]{12}$/) == null){
                $(this).css({"border-bottom-color":"red"}).parent().prev().css({"color":"red"});
            }else{
                $(this).removeAttr("style").parent().prev().removeAttr("style");
            }
        });

        $(this).keydown(function(e){
            console.log(e.keyCode);
            if(important_keys.indexOf(e.keyCode) == -1){
                if($(this).val().length == 0){
                    if(e.keyCode == "16"){
                        is16 = true;
                    }
                    if(e.keyCode == "107" || ((e.keyCode == "61" || e.keyCode == "187") && is16)){
                        is16 = false;
                    }else{
                        e.preventDefault();
                    }
                } else{
                    if($(this).val().length < 13 && keyCodes.indexOf(e.keyCode) != -1){
                    } else{
                        e.preventDefault();
                    }
                }
            }
        });
    });
    $(".check-email").each(function(){
        $(this).bind("blur.check",function(){
            if($(this).val().match(/^.{3,}\@.{1,}\..*$/) == null){
                $(this).css({"border-bottom-color":"red"}).parent().prev().css({"color":"red"});
            }else{
                $(this).removeAttr("style").parent().prev().removeAttr("style");
            }
        });
    });

    $("input[data-switchBlock]").each(function(){
        if(!$(this).is(":checked")){
            $($(this).attr("data-switchBlock")).hide()
        }else{
            $($(this).attr("data-switchBlock")).show()
        }
    }).on("change",function(){
        if(!$(this).is(":checked")){
            $($(this).attr("data-switchBlock")).hide()
        }else{
            $($(this).attr("data-switchBlock")).show()
        }        
    });

    if($("input[name='priceCat']").length>0){
        if($("input[name='priceCat']").val() == "r4"){
            $("#paramsDetails").show();
        }else{
            $("#paramsDetails").hide();
        }

        $("input[name='priceCat']").change(function(){
            if($(this).val() == "showParams"){
                $("#paramsDetails").show();
            }else{
                $("#paramsDetails").hide();
            }            
        });
    }

    $(".showModal").bind("click.modal",function(){
        var me = $(this);
        $("#modalsWrapper").fadeIn(200,function(){
            $(me.attr("data-modal")).fadeIn(150);
        });
        return false;
    });

    $(".closeModal").click(function(){
        $(this).parents(".modal").fadeOut(150,function(){
            $("#modalsWrapper").fadeOut(150);
        });
    });

    if($("#isCompany").is(":checked")){
        $(".privateLabel").hide();
        $(".companyLabel").css("display","inline-block");
    }
    if(!$("#isCompany").is(":checked")){
        $(".companyLabel").hide();
        $(".privateLabel").css("display","inline-block");
    }
    $("#isCompany").change(function(){
        if($(this).is(":checked")){
            $(".privateLabel").hide();
            $(".companyLabel").css("display","inline-block");
        }
        else{
            $(".companyLabel").hide();
            $(".privateLabel").css("display","inline-block");
        }
    });

    $(".switchLink").bind("click.switch",function(){
        var me = $(this);
        $(this).parents(".contentBlock").fadeOut(150,function(){
            $(me.attr("href")).fadeIn(200);
        });
        return false;
    });
});